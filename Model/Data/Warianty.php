<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Model\Data;

use Kowal\Warianty\Api\Data\WariantyInterface;

class Warianty extends \Magento\Framework\Api\AbstractExtensibleObject implements WariantyInterface
{

    /**
     * Get warianty_id
     * @return string|null
     */
    public function getWariantyId()
    {
        return $this->_get(self::WARIANTY_ID);
    }

    /**
     * Set warianty_id
     * @param string $wariantyId
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setWariantyId($wariantyId)
    {
        return $this->setData(self::WARIANTY_ID, $wariantyId);
    }

    /**
     * Get wariant_name
     * @return string|null
     */
    public function getWariantName()
    {
        return $this->_get(self::WARIANT_NAME);
    }

    /**
     * Set wariant_name
     * @param string $wariantName
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setWariantName($wariantName)
    {
        return $this->setData(self::WARIANT_NAME, $wariantName);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\Warianty\Api\Data\WariantyExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\Warianty\Api\Data\WariantyExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\Warianty\Api\Data\WariantyExtensionInterface $extensionAttributes
    )
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get skus
     * @return string|null
     */
    public function getSkus()
    {
        return $this->_get(self::SKUS);
    }

    /**
     * Set skus
     * @param string $skus
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setSkus($skus)
    {
        return $this->setData(self::SKUS, $skus);
    }

    /**
     * Get grupa
     * @return string|null
     */
    public function getGrupa()
    {
        return $this->_get(self::GRUPA);
    }

    /**
     * Set grupa
     * @param string $grupa
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setGrupa($grupa)
    {
        return $this->setData(self::GRUPA, $grupa);
    }

    /**
     * Get swatch
     * @return string|null
     */
    public function getSwatch()
    {
        return $this->_get(self::SWATCH);
    }

    /**
     * Set swatch
     * @param string $swatch
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setSwatch($swatch)
    {
        return $this->setData(self::SWATCH, $swatch);
    }

    /**
     * Get maingroup
     * @return string|null
     */
    public function getMaingroup()
    {
        return $this->_get(self::MAINGROUP);
    }

    /**
     * Set maingroup
     * @param string $maingroup
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setMaingroup($maingroup)
    {
        return $this->setData(self::MAINGROUP, $maingroup);
    }

    /**
     * Get display_as
     * @return string|null
     */
    public function getDisplayAs()
    {
        return $this->_get(self::DISPLAY_AS);
    }

    /**
     * Set display_as
     * @param string $displayAs
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setDisplayAs($displayAs)
    {
        return $this->setData(self::DISPLAY_AS, $displayAs);
    }


    /**
     * Get display_selected
     * @return string|null
     */
    public function getDisplaySelected()
    {
        return $this->_get(self::DISPLAY_SELECTED);
    }

    /**
     * Set display_selected
     * @param string $displaySelected
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setDisplaySelected($displaySelected)
    {
        return $this->setData(self::DISPLAY_SELECTED, $displaySelected);
    }

    /**
     * Get label
     * @return string|null
     */
    public function getLabel()
    {
        return $this->_get(self::LABEL);
    }

    /**
     * Set label
     * @param string $label
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setLabel($label)
    {
        return $this->setData(self::LABEL, $label);
    }
}

