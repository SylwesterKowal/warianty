<?php

namespace Kowal\Warianty\Model\Grupa;

/**
 * Class AttributeOptions
 * @package Kowal\Warianty\Model\Grupa
 */
class Attribute implements \Magento\Framework\Option\ArrayInterface
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory */
    private $collectionFactory;

    /** @var \Kowal\Warianty\Model\Grupa\AttributeScope */
    private $scope;

    /** @var array */
    private $items;

    /**
     * @var \Kowal\Warianty\Helper\Data
     */
    private $dataHelper;

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     * @param \Kowal\Warianty\Model\Grupa\AttributeScope $scope
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory,
        \Kowal\Warianty\Model\Grupa\AttributeScope                               $scope,
        \Kowal\Warianty\Helper\Data                                              $dataHelper
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->scope = $scope;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (is_null($this->items)) {
            $this->items = $this->getOptions();
        }

        return $this->items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute[]|\Magento\Framework\DataObject[]
     */
    public function getAttributes()
    {

        $collection = $this->getCollection();
        return $collection->getItems();
    }


    /**
     * @return array
     */
    private function getOptions()
    {
        $items = [];
        foreach ($this->getAttributes() as $attribute) {
            if ($attribute->getFrontendInput() == 'select' || $attribute->getFrontendInput() == 'multiselect') {
                $items[] = [
                    'label' => $attribute->getStoreLabel(), 'value' => $attribute->getName(),
                ];
            }
        }
        return $items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    private function getCollection()
    {
        return $this->collectionFactory->create();
    }
}
