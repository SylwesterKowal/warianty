<?php

namespace Kowal\Warianty\Model\Grupa;

/**
 * Class DataProvider
 * @package Kowal\Warianty\Model\Grupa
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $collection;

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory,
                                                                                 $primaryFieldName,
                                                                                 $requestFieldName,
        array                                                                    $meta = [],
        array                                                                    $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        parent::__construct('', $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [];
    }
}