<?php

namespace Kowal\Warianty\Model\Grupa;

/**
 * Class Mapper
 * @package Kowal\Warianty\Model\Grupa
 */
class Mapper
{
    /** @var array */
    public $map;

    /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute[] */
    private $attributes;

    /**
     * @param \Kowal\Warianty\Model\Grupa\Attribute $attribues
     */
    public function __construct(
        \Kowal\Warianty\Model\Grupa\Attribute $attribues
    )
    {
        $this->attributes = $attribues;
        $this->map = $this->makeMap();
    }

    /**
     * @return array
     */
    private function makeMap()
    {
        if (is_null($this->map)) {
            $this->map = $this->buildMap();
        }

        return $this->map;
    }

    /**
     * @return array
     */
    private function buildMap()
    {
        $items = [];
        foreach ($this->attributes->getAttributes() as $attribute) {
            $options = [];
            foreach ($attribute->getOptions() as $option) {
                if (empty($option->getValue())) {
                    continue;
                }
                $options[] = [
                    'label' => $option->getLabel(), 'value' => $option->getValue()
                ];
            }
            $items[$attribute->getName()] = $options;
        }

        return $items;
    }
}