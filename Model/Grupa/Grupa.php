<?php

namespace Kowal\Warianty\Model\Grupa;

/**
 * Class AttributeOptions
 * @package Kowal\Warianty\Model\Grupa
 */

class Grupa implements \Magento\Framework\Option\ArrayInterface
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory */
    private $collectionFactory;

    /** @var \Kowal\Warianty\Model\Grupa\AttributeScope */
    private $scope;

    /** @var array */
    private $items;

    /**
     * @var \Kowal\Warianty\Helper\Data
     */
    private $dataHelper;

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     * @param AttributeScope $scope
     * @param \Kowal\Warianty\Helper\Data $dataHelper
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory,
        \Kowal\Warianty\Model\Grupa\AttributeScope                               $scope,
        \Kowal\Warianty\Helper\Data                                              $dataHelper
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->scope = $scope;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (is_null($this->items)) {
            $this->items = $this->getOptions();
        }

        return $this->items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute[]|\Magento\Framework\DataObject[]
     */
    public function getAttributes()
    {
//        $codes = $this->scope->getCodes();
        $collection = $this->getCollection();
        if (!is_array($this->dataHelper->getGeneralCfg('codes'))) {
            $codes = explode(",", $this->dataHelper->getGeneralCfg('codes'));
            if (!empty($codes)) {
                $collection->addFieldToFilter('attribute_code', $codes);
            }
        }
        return $collection->getItems();
    }


    /**
     * @return array
     */
    private function getOptions()
    {
//        $items = ['label' => ' ','value' => ' ']; // !ERR wywala Add New Wariant
        foreach ($this->getAttributes() as $attribute) {
            if ($attribute->getFrontendInput() == 'select' || $attribute->getFrontendInput() == 'multiselect') {
                $items[] = [
                    'label' => $attribute->getStoreLabel(), 'value' => $attribute->getName(),
                ];
            }
        }
        return $items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    private function getCollection()
    {
        return $this->collectionFactory->create();
    }
}
