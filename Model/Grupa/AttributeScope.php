<?php

namespace Kowal\Warianty\Model\Grupa;

/**
 * Class AttributeScope
 * @package Kowal\Warianty\Model\Grupa
 */
class AttributeScope
{
    /** @var array */
    private $codes;

    /**
     * @param array $codes
     */
    public function __construct(
        $codes = []
    )
    {
        $this->codes = $codes;
    }

    /**
     * Get attribute codes to scope in form select element
     *
     * @return array
     */
    public function getCodes()
    {
        return $this->codes;
    }
}
