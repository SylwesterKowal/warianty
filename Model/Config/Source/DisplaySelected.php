<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Model\Config\Source;

class DisplaySelected implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '1', 'label' => __('Yes')], ['value' => '0', 'label' => __('No')]];
    }

    public function toArray()
    {
        return ['1' => __('Yes'), '0' => __('No')];
    }
}
