<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Model\Config\Source;

class Limit implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'all', 'label' => __('all')], ['value' => '1', 'label' => __('1')], ['value' => '2', 'label' => __('2')], ['value' => '3', 'label' => __('3')], ['value' => '4', 'label' => __('4')], ['value' => '5', 'label' => __('5')], ['value' => '6', 'label' => __('6')], ['value' => '7', 'label' => __('7')], ['value' => '8', 'label' => __('8')], ['value' => '9', 'label' => __('9')], ['value' => '10', 'label' => __('10')], ['value' => '11', 'label' => __('11')], ['value' => '12', 'label' => __('12')], ['value' => '13', 'label' => __('13')], ['value' => '14', 'label' => __('14')], ['value' => '15', 'label' => __('15')], ['value' => '16', 'label' => __('16')], ['value' => '17', 'label' => __('17')], ['value' => '18', 'label' => __('18')], ['value' => '19', 'label' => __('19')], ['value' => '20', 'label' => __('20')]];
    }

    public function toArray()
    {
        return ['all' => __('all'), '1' => __('1'), '2' => __('2'), '3' => __('3'), '4' => __('4'), '5' => __('5'), '6' => __('6'), '7' => __('7'), '8' => __('8'), '9' => __('9'), '10' => __('10'), '11' => __('11'), '12' => __('12'), '13' => __('13'), '14' => __('14'), '15' => __('15'), '16' => __('16'), '17' => __('17'), '18' => __('18'), '19' => __('19'), '20' => __('20')];
    }
}

