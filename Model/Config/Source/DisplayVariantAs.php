<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Model\Config\Source;

class DisplayVariantAs implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'swatch', 'label' => __('swatch')], ['value' => 'dropdown', 'label' => __('dropdown')]];
    }

    public function toArray()
    {
        return ['swatch' => __('swatch'), 'dropdown' => __('dropdown')];
    }
}
