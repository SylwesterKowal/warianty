<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Kowal\Warianty\Model\ResourceModel\Warianty;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'warianty_id';

    protected $_init;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\Warianty\Model\Warianty::class,
            \Kowal\Warianty\Model\ResourceModel\Warianty::class
        );
    }
}

