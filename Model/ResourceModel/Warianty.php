<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Kowal\Warianty\Model\ResourceModel;

class Warianty extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected $_init;
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('kowal_warianty_warianty', 'warianty_id');
    }
}

