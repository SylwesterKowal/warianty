<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Model;

use Kowal\Warianty\Api\Data\WariantyInterface;
use Kowal\Warianty\Api\Data\WariantyInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Warianty extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'kowal_warianty_warianty';
    protected $wariantyDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param WariantyInterfaceFactory $wariantyDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\Warianty\Model\ResourceModel\Warianty $resource
     * @param \Kowal\Warianty\Model\ResourceModel\Warianty\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context                        $context,
        \Magento\Framework\Registry                             $registry,
        WariantyInterfaceFactory                                $wariantyDataFactory,
        DataObjectHelper                                        $dataObjectHelper,
        \Kowal\Warianty\Model\ResourceModel\Warianty            $resource,
        \Kowal\Warianty\Model\ResourceModel\Warianty\Collection $resourceCollection,
        array                                                   $data = []
    )
    {
        $this->wariantyDataFactory = $wariantyDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve warianty model with warianty data
     * @return WariantyInterface
     */
    public function getDataModel()
    {
        $wariantyData = $this->getData();

        $wariantyDataObject = $this->wariantyDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $wariantyDataObject,
            $wariantyData,
            WariantyInterface::class
        );

        return $wariantyDataObject;
    }
}

