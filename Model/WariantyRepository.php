<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Model;

use Kowal\Warianty\Api\Data\WariantyInterfaceFactory;
use Kowal\Warianty\Api\Data\WariantySearchResultsInterfaceFactory;
use Kowal\Warianty\Api\WariantyRepositoryInterface;
use Kowal\Warianty\Model\ResourceModel\Warianty as ResourceWarianty;
use Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory as WariantyCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class WariantyRepository implements WariantyRepositoryInterface
{

    private $collectionProcessor;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $wariantyFactory;

    protected $dataObjectProcessor;

    protected $searchResultsFactory;

    private $storeManager;

    protected $wariantyCollectionFactory;

    protected $dataWariantyFactory;

    protected $extensionAttributesJoinProcessor;

    protected $dataObjectHelper;


    /**
     * @param ResourceWarianty $resource
     * @param WariantyFactory $wariantyFactory
     * @param WariantyInterfaceFactory $dataWariantyFactory
     * @param WariantyCollectionFactory $wariantyCollectionFactory
     * @param WariantySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceWarianty                      $resource,
        WariantyFactory                       $wariantyFactory,
        WariantyInterfaceFactory              $dataWariantyFactory,
        WariantyCollectionFactory             $wariantyCollectionFactory,
        WariantySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper                      $dataObjectHelper,
        DataObjectProcessor                   $dataObjectProcessor,
        StoreManagerInterface                 $storeManager,
        CollectionProcessorInterface          $collectionProcessor,
        JoinProcessorInterface                $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter         $extensibleDataObjectConverter
    )
    {
        $this->resource = $resource;
        $this->wariantyFactory = $wariantyFactory;
        $this->wariantyCollectionFactory = $wariantyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataWariantyFactory = $dataWariantyFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\Warianty\Api\Data\WariantyInterface $warianty
    )
    {
        /* if (empty($warianty->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $warianty->setStoreId($storeId);
        } */

        $wariantyData = $this->extensibleDataObjectConverter->toNestedArray(
            $warianty,
            [],
            \Kowal\Warianty\Api\Data\WariantyInterface::class
        );

        $wariantyModel = $this->wariantyFactory->create()->setData($wariantyData);

        try {
            $this->resource->save($wariantyModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the warianty: %1',
                $exception->getMessage()
            ));
        }
        return $wariantyModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($wariantyId)
    {
        $warianty = $this->wariantyFactory->create();
        $this->resource->load($warianty, $wariantyId);
        if (!$warianty->getId()) {
            throw new NoSuchEntityException(__('Warianty with id %1 does not exist.', $wariantyId));
        }
        return $warianty->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    )
    {
        $collection = $this->wariantyCollectionFactory->create();


        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\Warianty\Api\Data\WariantyInterface::class
        );


        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\Warianty\Api\Data\WariantyInterface $warianty
    )
    {
        try {
            $wariantyModel = $this->wariantyFactory->create();
            $this->resource->load($wariantyModel, $warianty->getWariantyId());
            $this->resource->delete($wariantyModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Warianty: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($wariantyId)
    {
        return $this->delete($this->get($wariantyId));
    }
}

