<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Plugin\Backend\Magento\Catalog\Model;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory as WariantyCollectionFactory;

class Product
{

    protected $collectionFactory;
    protected $wariantyCollectionFactory;

    public function __construct(
        CollectionFactory         $collectionFactory,
        WariantyCollectionFactory $wariantyCollectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->wariantyCollectionFactory = $wariantyCollectionFactory;
    }

    public function beforeSave(
        \Magento\Catalog\Model\Product $subject
    )
    {


        $collection = $this->collectionFactory->create();
        $collection->addAttributeToFilter('sku', ['eq' => $subject->getSku()]);

        // usuwamy skus z wariantów
        foreach ($collection as $product) {
            if (!$product->getWariant()) continue;
            if ($old_warianty = explode(",", $product->getWariant())) {
                $wariantyCollection = $this->wariantyCollectionFactory->create();
                $wariantyCollection->addFieldToFilter('warianty_id', ['in' => $old_warianty]);
                foreach ($wariantyCollection as $wariant) {
                    $skus = explode(PHP_EOL, $wariant->getSkus());
                    $newSkus = [];
                    foreach ($skus as $sku) {
                        if ($sku == $product->getSku()) continue;
                        $newSkus[] = $sku;
                    }
                    $newSkus = implode(PHP_EOL, $newSkus);
                    $wariant->setSkus($newSkus);
                    $wariant->save();
                }
            }
        }

        if ($new_warianty = $subject->getWariant()) {

            $wariantyCollection = $this->wariantyCollectionFactory->create();
            $wariantyCollection->addFieldToFilter('warianty_id', ['in' => $new_warianty]);
            foreach ($wariantyCollection as $wariant) {
                $skus = explode(PHP_EOL, $wariant->getSkus());
                if (!in_array($product->getSku(), $skus)) {
                    $skus[] = $product->getSku();
                    $newSkus = implode(PHP_EOL, $skus);
                    $wariant->setSkus($newSkus);
                    $wariant->save();
                }
            }
        }

        return [];
    }

}

