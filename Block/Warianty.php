<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Block;

class Warianty extends \Magento\Framework\View\Element\Template
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param \Magento\Framework\Registry $registry
     * @param \Kowal\Warianty\Model\Warianty $warianty
     * @param \Kowal\Warianty\Helper\Data $dataHelper
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Catalog\Helper\Image $image
     * @param \Magento\Swatches\Helper\Media $swatchHelper
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $productAttributeRepository
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context               $context,
        \Magento\Framework\Registry                                    $registry,
        \Kowal\Warianty\Model\Warianty                                 $warianty,
        \Kowal\Warianty\Helper\Data                                    $dataHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Helper\Image                                  $image,
        \Magento\Swatches\Helper\Media                                 $swatchHelperMedia,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface       $productAttributeRepository,
        \Magento\Eav\Model\Config                                      $eavConfig,
        \Magento\Swatches\Helper\Data                                  $swatchHelperData,
        array                                                          $data = []
    )
    {
        $this->_registry = $registry;
        $this->warianty = $warianty;
        $this->dataHelper = $dataHelper;
        $this->image = $image;
        $this->swatchHelper = $swatchHelperMedia;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->collectionFactory = $collectionFactory;
        $this->eavConfig = $eavConfig;
        $this->swatchHelperData = $swatchHelperData;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getWariants($currentSku)
    {
        if ($warianty_ = $this->getWariantId()) {
//            $warianty = explode(",", $warianty_);
            $warianty = array_values(array_filter(explode(",", $warianty_)));
            if (is_array($warianty)) {
                $groups_wariant = [];
                foreach ($warianty as $warianty_id) {
                    if ($wariant = $this->warianty->load(trim($warianty_id))) {
                        if ($skus = $wariant->getSkus()) {

                            $skus_ = (!is_null($skus)) ? array_values(array_filter(explode(PHP_EOL, $skus))) : [];
                            $grupy = (!is_null($wariant->getGrupa())) ? array_values(array_filter(explode(",", $wariant->getGrupa()))) : [];
                            $swatch = (!is_null($wariant->getSwatch())) ? array_values(array_filter(explode(",", $wariant->getSwatch()))) : [];

                            $display_variant_as = $wariant->getDisplayAs();
                            $display_selected = $wariant->getDisplaySelected();

                            foreach ($skus_ as $key => $_sku) {
                                $skus_[$key] = (string)trim($_sku);
                            }

                            if ($maingroup = $wariant->getMaingroup()) {

//                            file_put_contents("_warianty.txt", "Grupy " . print_r($grupy, true));
                                $collection = $this->collectionFactory->create();
                                $collection->addAttributeToFilter('sku', ['eq' => $currentSku]);
                                $collection->addAttributeToFilter('status', ['eq' => 1]);
                                $collection->addAttributeToSelect('*');
                                foreach ($collection as $product) {
                                    $grupa_value = $product->getResource()->getAttribute($maingroup)->getFrontend()->getValue($product);
                                }
                            }

                            if (is_array($skus_)) {
                                $collection = $this->collectionFactory->create();
                                $collection->addAttributeToFilter('sku', ['in' => $skus_]);
                                $collection->addAttributeToFilter('status', ['eq' => 1]);
                                $collection->addAttributeToSelect('*');
                                foreach ($collection as $product) {

//                                        file_put_contents("_warianty.txt", "\nSKU " . print_r($product->getSku(), true), FILE_APPEND);
                                    if (is_array($grupy) && !empty($grupy) && !empty($maingroup)) {
                                        foreach ($grupy as $grupa_) {
                                            $product_data = $product->getData($grupa_);
                                            //file_put_contents("_warianty.txt", "\nGRUPA " . print_r($product_data, true), FILE_APPEND);
                                            if ($product_data && $maingroup == $grupa_) {

                                                $label = $this->getLabel($product->getResource()->getAttribute($grupa_)->getStoreLabel($product), $wariant);
                                                $value = $product->getResource()->getAttribute($grupa_)->getFrontend()->getValue($product);


                                                $groups_wariant[$warianty_id][$grupa_]['products'][$value] = $product;
//                                                $groups_wariant[$warianty_id][$grupa_]['products']  = ksort($groups_wariant[$warianty_id][$grupa_]['products']);
                                                $groups_wariant[$warianty_id][$grupa_]['label'] = $label;
                                                $groups_wariant[$warianty_id][$grupa_]['swatch'] = in_array($grupa_, $swatch);
                                                $groups_wariant[$warianty_id][$grupa_]['display_variant_as'] = $display_variant_as;
                                                $groups_wariant[$warianty_id][$grupa_]['display_selected'] = $display_selected;
                                            }
                                        }

                                        foreach ($grupy as $swatch_) {
                                            $product_data = $product->getData($swatch_);
                                            //file_put_contents("_warianty.txt", "\nGRUPA SWATCH" . print_r($product_data, true), FILE_APPEND);
                                            if ($product_data && $maingroup != $grupa_) {

                                                $label = $this->getLabel($product->getResource()->getAttribute($swatch_)->getStoreLabel($product), $wariant);;
                                                $value = $product->getResource()->getAttribute($swatch_)->getFrontend()->getValue($product);


                                                if ($grupa_value == $value) {
                                                    $groups_wariant[$warianty_id][$swatch_]['products'][$value] = $product;
                                                    $groups_wariant[$warianty_id][$swatch_]['label'] = $label;
                                                    $groups_wariant[$warianty_id][$swatch_]['swatch'] = in_array($grupa_, $swatch);
                                                    $groups_wariant[$warianty_id][$swatch_]['display_variant_as'] = $display_variant_as;
                                                    $groups_wariant[$warianty_id][$swatch_]['display_selected'] = $display_selected;
                                                }
                                            }
                                        }
                                    } else {
                                        $groups_wariant[$warianty_id]['default']['products'][] = $product;
                                        $groups_wariant[$warianty_id]['default']['display_variant_as'] = $display_variant_as;
                                        $groups_wariant[$warianty_id]['default']['label'] = $wariant->getLabel();
                                        $groups_wariant[$warianty_id]['default']['display_selected'] = $display_selected;
                                    }
                                }
                            }
                        }
                    }
                }
                return $groups_wariant;
            }
            return null;
        } else {
            return null;
        }
    }

    private function getLabel($product_label, $wariant)
    {
        if (!$product_label) {
            return $wariant->getLabel();
        }else{
            return $product_label;
        }
    }

    public function getImage($product)
    {
        $imageUrl = $this->image->init($product, 'product_page_image_small')
            ->setImageFile($product->getSmallImage()) // image,small_image,thumbnail
            ->resize($this->dataHelper->getGeneralCfg('image_size'))
            ->getUrl();
        return $imageUrl;
    }

    public function getDefaultLabel(){
        return $this->dataHelper->getGeneralCfg('default_label');
    }

    private function getWariantId()
    {
        return $this->_registry->registry('current_product')->getWariant();
    }

    public function getCurentProductSku()
    {
        return $this->_registry->registry('current_product')->getSku();
    }

    public function getSwatchImages($attribute_code)
    {
        $optionsImages = [];
        if ($this->isSwatchAttr($attribute_code)) {
            $options = $this->productAttributeRepository->get($attribute_code);
            //file_put_contents("_" . $attribute_code . ".txt", print_r($options->getFrontendInput(), true));

            foreach ($options->getOptions() as $option) {
                $name = $option->getLabel();
                $optId = $option->getValue();

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $swatchCollection = $objectManager->create('Magento\Swatches\Model\ResourceModel\Swatch\Collection');
                $swatchCollection->addFieldtoFilter('option_id', $optId);
                $item = $swatchCollection->getFirstItem();

//    $SwatchImage =  $swatchHelper->getSwatchAttributeImage('swatch_image', $item->getValue());
                if ($SwatchImage = $this->swatchHelper->getSwatchAttributeImage('swatch_thumb', $item->getValue())) {
                    // if ($this->checkImagePath($SwatchImage)) {
                    $optionsImages[$optId] = [
                        'image' => $SwatchImage,
                        'label' => $name
                    ];
                    // }
                }

            }
        }
        return $optionsImages;
    }

    /**
     * Działa ale bardzo wolno.!!!!!!
     * @param $url
     * @return bool
     */
    public function checkImagePath($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ($code == 200);


    }

    public function dashesToCamelCase($string, $capitalizeFirstCharacter = true)
    {

        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }


    public function isSwatchAttr($att)
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', $att);
        return $this->swatchHelperData->isSwatchAttribute($attribute);
    }

    public function displayVariantAs($displayAs)
    {
        if (!empty($displayAs) && $displayAs == 'dropdown') {
            return 'dropdown';
        } else if (!empty($displayAs) && $displayAs == 'swatch') {
            return 'swatch';
        } else {
            $displayVariantAs = $this->dataHelper->getGeneralCfg('display_variant_as');
            if ($displayVariantAs == 'dropdown') {
                return 'dropdown';
            } else {
                return 'swatch';
            }
        }
    }
}

