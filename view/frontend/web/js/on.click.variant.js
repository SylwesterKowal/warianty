
define([
    'jquery',
    'domReady!'
], function ($) {
    'use strict';


    return function (dateString, element) {
        $(element).on('change', function() {
            console.log( this.value );
            console.log( dateString );
            if(dateString.blank == 1){
                window.open(this.value, '_blank');
            }else{
                window.location.href = this.value;
            }

        });
    };
});
