define([
    'uiRegistry',
    'Magento_Ui/js/form/element/multiselect'
], function (
    uiRegistry,
    Multiselect
) {
    'use strict';

    return Multiselect.extend({

        defaults: {
            mapper: []
        },

        initialize: function () {
            this._super();
            return this.setDependentOptions(this.value());
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {
            this.setDependentOptions(value);
            return this._super();
        },

        /**
         * Set options to dependent select
         *
         * @param {String} value
         */
        setDependentOptions: function (value) {

            var swatch = uiRegistry.get('index = swatch');
            var new_options = [];
            value.forEach(function (item1) {
                swatch.initialOptions.forEach(function (item2) {

                    if (item2.value == item1) {
                        new_options.push(item2);
                    }
                });
            });

            swatch.setOptions(new_options);


            var maingroup = uiRegistry.get('index = maingroup');
            var new_options = [];
            value.forEach(function (item1) {
                maingroup.initialOptions.forEach(function (item2) {

                    if (item2.value == item1) {
                        new_options.push(item2);
                    }
                });
            });

            maingroup.setOptions(new_options);
            return this;
        }
    });
});