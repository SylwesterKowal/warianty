<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Controller\Adminhtml\Warianty;

class Edit extends \Kowal\Warianty\Controller\Adminhtml\Warianty
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context        $context,
        \Magento\Framework\Registry                $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('warianty_id');
        $model = $this->_objectManager->create(\Kowal\Warianty\Model\Warianty::class);
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Warianty no longer exists.'));

                return $resultRedirect->setPath('*/*/');
            } elseif ($id == 1) {
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('kowal_warianty_warianty', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Warianty') : __('New Warianty'),
            $id ? __('Edit Warianty') : __('New Warianty')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Wariantys'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Warianty %1', $model->getId()) : __('New Warianty'));
        return $resultPage;
    }
}

