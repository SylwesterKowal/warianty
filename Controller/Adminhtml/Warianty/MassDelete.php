<?php

namespace Kowal\Warianty\Controller\Adminhtml\Warianty;

use Exception;
use Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory;
use Kowal\Warianty\Model\Warianty;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;

class MassDelete extends Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    protected $collection;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param Collection $collection
     */
    public function __construct(
        Context           $context,
        Filter            $filter,
        CollectionFactory $collectionFactory,
        Collection        $collection
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collection;
    }

    /**
     * Execute action
     *
     * @return Redirect
     * @throws LocalizedException|Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        foreach ($collection as $item) {
            // init model and delete
            if ($id = $item->getId()) {
                $model = $this->_objectManager->create(Warianty::class);
                $model->load($id);
                $item->delete();
                $this->deleteWariantInProducts($id, $model->getSkus());
            }
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }

    public function deleteWariantInProducts($wariant_id, $skus)
    {
        $skus = $this->getSkus($skus);
        if ($products = $this->getCollection($skus)) {
            foreach ($products as $product_) {
                try {
                    if ($options = $product_->getWariant()) {
                        $options = array_filter(explode(',', $options));

                        if (($key = array_search($wariant_id, $options)) !== false) {
                            unset($options[$key]);
                        }
                        $product_->addAttributeUpdate('wariant', implode(',', $options), 0);
                    }
                } catch (Exception $e) {
//                    file_put_contents("_wariant_" . $product_->getSku() . ".txt", $e->getMessage());
                }
            }
        }
    }

    public function getSkus($skus)
    {
        $skus = array_values(array_filter(explode(PHP_EOL, $skus)));
        foreach ($skus as $key => $_sku) {
            $skus[$key] = (string)trim($_sku);
        }
    }

    public function getCollection($skus)
    {

        return $this->collection->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', ['in' => $skus])
            ->load();

    }
}

