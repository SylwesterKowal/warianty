<?php

namespace Kowal\Warianty\Controller\Adminhtml\Warianty;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory as WariantyCollectionFactory;


class MassAdd extends Action
{

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $prodCollFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /*
     * @var \Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory
     */
    protected $wariantyCollectionFactory;

    /*
     *  @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $prodCollFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param WariantyCollectionFactory $wariantyCollectionFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        Context                                         $context,
        Filter                                          $filter,
        CollectionFactory                               $prodCollFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        WariantyCollectionFactory                       $wariantyCollectionFactory,
        \Magento\Framework\App\RequestInterface         $request
    )
    {
        $this->filter = $filter;
        $this->prodCollFactory = $prodCollFactory;
        $this->productRepository = $productRepository;
        $this->wariantyCollectionFactory = $wariantyCollectionFactory;
        $this->request = $request;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException | \Exception
     */
    public function execute()
    {
        if ($new_wariants_id = $this->createNewWariant()) {
            $skus = [];
            $collection = $this->filter->getCollection($this->prodCollFactory->create());
            foreach ($collection->getAllIds() as $productId) {
                $productDataObject = $this->productRepository->getById($productId);
                if ($wariants = (string)$productDataObject->getWariant()) {

                    if ($curent_wariants = explode(",", $wariants)) {
                        if (is_array($curent_wariants)) {
                            array_push($curent_wariants, $new_wariants_id);
                            $wariants = implode(",", $curent_wariants);
                        } else {
                            $wariants = implode(",", [$curent_wariants, $new_wariants_id]);
                        }
                    } else {
                        $wariants = $new_wariants_id;
                    }
                } else {
                    $wariants = $new_wariants_id;
                }

                $productDataObject->setData('wariant', $wariants);
                if ($this->productRepository->save($productDataObject)) {
                    $skus[] = $productDataObject->getSku();
                } else {
                    $this->messageManager->addWorning(__('Error adding Wariant for product SKU', $productDataObject->getSku()));
                }
            }

            $wariantyCollection = $this->wariantyCollectionFactory->create();
            $wariantyCollection->addFieldToFilter('warianty_id', ['eq' => $new_wariants_id]);
            foreach ($wariantyCollection as $wariant) {

                $newSkus = implode(PHP_EOL, $skus);
                $wariant->setSkus($newSkus);
                $wariant->setDisplayAs('swatch');
                $wariant->save();

            }
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been modified.', $collection->getSize()));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('catalog/product/index');
    }

    private function createNewWariant()
    {
        $displayas = $this->request->getParam('displayas');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create(\Kowal\Warianty\Model\Warianty::class)->load(null);
        $data = ['wariant_name' => date('Ymd-His') . ' wariant swatch'];
        $model->setData($data);
        $model->save();
        return $model->getId();
    }
}