<?php

namespace Kowal\Warianty\Controller\Adminhtml\Warianty;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory as WariantyCollectionFactory;

class MassRemove extends Action
{

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $prodCollFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory
     */
    protected $wariantyCollectionFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $prodCollFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param WariantyCollectionFactory $wariantyCollectionFactory
     */
    public function __construct(
        Context                                         $context,
        Filter                                          $filter,
        CollectionFactory                               $prodCollFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        WariantyCollectionFactory                       $wariantyCollectionFactory
    )
    {
        $this->filter = $filter;
        $this->prodCollFactory = $prodCollFactory;
        $this->productRepository = $productRepository;
        $this->wariantyCollectionFactory = $wariantyCollectionFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException | \Exception
     */
    public function execute()
    {

        $skus = [];
        $collection = $this->filter->getCollection($this->prodCollFactory->create());
        foreach ($collection->getAllIds() as $productId) {
            $productDataObject = $this->productRepository->getById($productId);
            $curent_wariants = explode(",", $productDataObject->getWariant());

            $productDataObject->setData('wariant', "");
            $this->productRepository->save($productDataObject);

            $skus[] = $productDataObject->getSku();
        }

        $wariantyCollection = $this->wariantyCollectionFactory->create();
        $wariantyCollection->addFieldToFilter('warianty_id', ['in' => $curent_wariants]);
        foreach ($wariantyCollection as $wariant) {
            $old_skus = explode(PHP_EOL, $wariant->getSkus());
            $new_skus = [];
            foreach ($old_skus as $sku) {
                if (!in_array($sku, $skus)) {
                    $new_skus[] = $sku;
                }
            }
            $newSkus = implode(PHP_EOL, $new_skus);
            $wariant->setSkus($newSkus);
            $wariant->save();

        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been modified.', $collection->getSize()));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('catalog/product/index');
    }
}