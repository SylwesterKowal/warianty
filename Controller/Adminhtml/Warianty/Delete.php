<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Controller\Adminhtml\Warianty;

class Delete extends \Kowal\Warianty\Controller\Adminhtml\Warianty
{
    protected $_coreRegistry;
    protected $collection;

    public function __construct(
        \Magento\Backend\App\Action\Context                     $context,
        \Magento\Framework\Registry                             $coreRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    )
    {
        $this->collection = $collection;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('warianty_id');
        if ($id && $id != 1) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Kowal\Warianty\Model\Warianty::class);
                $model->load($id);
                $model->delete();
                $this->deleteWariantInProducts($id, $model->getSkus());
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Warianty.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['warianty_id' => $id]);
            }
        } else {
            return $resultRedirect->setPath('*/*/');
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Warianty to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }


    public function deleteWariantInProducts($wariant_id, $skus)
    {
        $skus = $this->getSkus($skus);
        if ($products = $this->getCollection($skus)) {
            foreach ($products as $product_) {
                try {
                    if ($options = $product_->getWariant()) {
                        $options = array_filter(explode(',', $options));

                        if (($key = array_search($wariant_id, $options)) !== false) {
                            unset($options[$key]);
                        }
                        $product_->addAttributeUpdate('wariant', implode(',', $options), 0);
                    }
                } catch (\Exception $e) {
//                    file_put_contents("_wariant_" . $product_->getSku() . ".txt", $e->getMessage());
                }
            }
        }
    }

    public function getSkus($skus)
    {
        $skus = array_values(array_filter(explode(PHP_EOL, $skus)));
        foreach ($skus as $key => $_sku) {
            $skus[$key] = (string)trim($_sku);
        }
    }

    public function getCollection($skus)
    {

        return $this->collection->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', ['in' => $skus])
            ->load();

    }
}

