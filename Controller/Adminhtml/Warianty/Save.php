<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Controller\Adminhtml\Warianty;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;
    protected $collection;
    protected $productAction;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @param \Magento\Catalog\Model\Product\Action $productAction
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                     $context,
        \Magento\Framework\App\Request\DataPersistorInterface   $dataPersistor,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
        \Magento\Catalog\Model\Product\Action                   $productAction
    )
    {
        $this->dataPersistor = $dataPersistor;
        $this->collection = $collection;
        $this->productAction = $productAction;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (is_array($data['grupa'])) {
                $data['grupa'] = implode(",", $data['grupa']); // grupa jako multiselect
            }
            if (is_array($data['swatch'])) {
                $data['swatch'] = implode(",", $data['swatch']); // swatch jako multiselect
            }
            $id = $this->getRequest()->getParam('warianty_id');

            $model = $this->_objectManager->create(\Kowal\Warianty\Model\Warianty::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Warianty no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $oldSkus = $model->getSkus();
            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Warianty.'));
                $this->dataPersistor->clear('kowal_warianty_warianty');

                /**
                 * Przypisanie produktów do tego zapisywanego wariantu na bazie pola SKUS
                 */
                $this->setProductAsWariants($data, $model->getId(), $oldSkus);


                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['warianty_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Warianty.'));
            }

            $this->dataPersistor->set('kowal_warianty_warianty', $data);
            return $resultRedirect->setPath('*/*/edit', ['warianty_id' => $this->getRequest()->getParam('warianty_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function getCollection($skus)
    {

        return $this->collection->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', ['in' => $skus])
            ->load();

    }

    private function setProductAsWariants($data, $wariantId, $oldSkus = null)
    {
        $skus = $this->getSkusFromText($data['skus']);
        $old_skus = $this->getSkusFromText($oldSkus);
        $add_from_sku = array_diff($skus, $old_skus);
        $delete_from_sku = array_diff($old_skus, $skus);

        if ($products = $this->getCollection($add_from_sku)) {
            foreach ($products as $product_) {
                try {
                    if ($options = $product_->getWariant()) {
                        $options = array_filter(explode(',', $options));
                    }
                    $options[] = $wariantId;
                    $options = array_unique($options);
                    $product_->addAttributeUpdate('wariant', implode(',', $options), 0);
                } catch (\Exception $e) {
//                    file_put_contents("_wariant_" . $product_->getSku() . ".txt", $e->getMessage());
                }
            }
        }


        if ($products = $this->getCollection($delete_from_sku)) {
            foreach ($products as $product_) {
                try {
                    if ($options = $product_->getWariant()) {
                        $options = array_filter(explode(',', $options));

                        if (($key = array_search($wariantId, $options)) !== false) {
                            unset($options[$key]);
                        }
                        $product_->addAttributeUpdate('wariant', implode(',', $options), 0);
                    }
                } catch (\Exception $e) {
//                    file_put_contents("_wariant_" . $product_->getSku() . ".txt", $e->getMessage());
                }
            }
        }
    }

    private function getSkusFromText($skus)
    {
        $skus = array_values(array_filter(explode(PHP_EOL, (string)$skus)));
        foreach ($skus as $key => $_sku) {
            $skus[$key] = (string)trim($_sku);
        }
        return $skus;
    }

}

