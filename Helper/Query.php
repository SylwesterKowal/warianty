<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\Warianty\Helper;


use Magento\Framework\App\ResourceConnection;

class Query
{
    public $store_id = 0;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }


    public function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    public function checkIfSkuExists($sku, $producent = '')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT sku, type_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku IN (?,?)";
        return $connection->fetchRow($sql, [trim($sku), trim($producent) . '_' . trim($sku)]);
    }

    public function checkIfStockExists($stock_id)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT stock_id  FROM " . $this->_getTableName('inventory_stock') . " WHERE stock_id = ?";
        $result = $connection->fetchRow($sql, [$stock_id]);
        return ($result) ? $stock_id : 1;
    }


    public function updateAttrVarchar($sku, $value, $code)
    {
        $connection = $this->_getConnection('core_write');
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_varchar') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $value
            ]
        );
    }

    public function updateAttrInt($sku, $value, $code)
    {
        $connection = $this->_getConnection('core_write');
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_int') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $value
            ]
        );
    }

    public function updateAttrDate($sku, $delivery_date, $code)
    {
        $connection = $this->_getConnection('core_write');
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_datetime') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $delivery_date
            ]
        );
    }

    /**
     * @param $attributeCode
     * @return mixed
     */
    private function _getAttributeId($attributeCode)
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT attribute_id FROM " . $this->_getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $this->_getEntityTypeId('catalog_product'),
                $attributeCode
            ]
        );
    }

    /**
     * @param $entityTypeCode
     * @return mixed
     */
    private function _getEntityTypeId($entityTypeCode)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $entityTypeCode
            ]
        );
    }

    function getAttributeValue($productId, $attributeId, $attributeKey)
    {
        try {
            $attribute_type = $this->_getAttributeType($attributeKey);

            if ($attribute_type == 'static') return false;

            $connection = $this->_getConnection('core_write');

            $sql = "SELECT value FROM " . $this->_getTableName('catalog_product_entity_' . $attribute_type) . " cped
			WHERE  cped.attribute_id = ?
			AND cped.entity_id = ?";
            return $connection->fetchOne($sql, array($attributeId, $productId));

        } catch (Exception $e) {
            return $e->getMessage() . ' ' . $attributeKey;
        }
    }

    function _getAttributeType($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT backend_type
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId('catalog_product');
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }
}
