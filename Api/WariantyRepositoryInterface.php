<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface WariantyRepositoryInterface
{

    /**
     * Save Warianty
     * @param \Kowal\Warianty\Api\Data\WariantyInterface $warianty
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\Warianty\Api\Data\WariantyInterface $warianty
    );

    /**
     * Retrieve Warianty
     * @param string $wariantyId
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($wariantyId);

    /**
     * Retrieve Warianty matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\Warianty\Api\Data\WariantySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Warianty
     * @param \Kowal\Warianty\Api\Data\WariantyInterface $warianty
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\Warianty\Api\Data\WariantyInterface $warianty
    );

    /**
     * Delete Warianty by ID
     * @param string $wariantyId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($wariantyId);

}

