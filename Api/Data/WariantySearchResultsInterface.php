<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Api\Data;

interface WariantySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Warianty list.
     * @return \Kowal\Warianty\Api\Data\WariantyInterface[]
     */
    public function getItems();

    /**
     * Set wariant_name list.
     * @param \Kowal\Warianty\Api\Data\WariantyInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

