<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Api\Data;

interface WariantyInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const MAINGROUP = 'maingroup';
    const SKUS = 'skus';
    const WARIANTY_ID = 'warianty_id';
    const WARIANT_NAME = 'wariant_name';
    const GRUPA = 'grupa';
    const SWATCH = 'swatch';
    const DISPLAY_SELECTED = 'display_selected';
    const DISPLAY_AS = 'display_as';
    const LABEL = 'label';


    /**
     * Get warianty_id
     * @return string|null
     */
    public function getWariantyId();

    /**
     * Set warianty_id
     * @param string $wariantyId
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setWariantyId($wariantyId);

    /**
     * Get wariant_name
     * @return string|null
     */
    public function getWariantName();

    /**
     * Set wariant_name
     * @param string $wariantName
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setWariantName($wariantName);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\Warianty\Api\Data\WariantyExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\Warianty\Api\Data\WariantyExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\Warianty\Api\Data\WariantyExtensionInterface $extensionAttributes
    );

    /**
     * Get skus
     * @return string|null
     */
    public function getSkus();

    /**
     * Set skus
     * @param string $skus
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setSkus($skus);

    /**
     * Get grupa
     * @return string|null
     */
    public function getGrupa();

    /**
     * Set grupa
     * @param string $grupa
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setGrupa($grupa);

    /**
     * Get swatch
     * @return string|null
     */
    public function getSwatch();

    /**
     * Set swatch
     * @param string $swatch
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setSwatch($swatch);

    /**
     * Get maingroup
     * @return string|null
     */
    public function getMaingroup();

    /**
     * Set maingroup
     * @param string $maingroup
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setMaingroup($maingroup);

    /**
     * Get display_as
     * @return string|null
     */
    public function getDisplayAs();

    /**
     * Set display_as
     * @param string $displayAs
     * @return \Kowal\Warianty\Warianty\Api\Data\WariantyInterface
     */
    public function setDisplayAs($displayAs);

    /**
     * Get display_selected
     * @return string|null
     */
    public function getDisplaySelected();

    /**
     * Set display_selected
     * @param string $displaySelected
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setDisplaySelected($displaySelected);

    /**
     * Get label
     * @return string|null
     */
    public function getLabel();

    /**
     * Set label
     * @param string $label
     * @return \Kowal\Warianty\Api\Data\WariantyInterface
     */
    public function setLabel($label);
}

