<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Warianty\Observer\Catalog;

use Kowal\Warianty\Helper\Query;
use Kowal\Warianty\Model\ResourceModel\Warianty\CollectionFactory as WariantyCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class ProductAttributeUpdateBefore implements \Magento\Framework\Event\ObserverInterface
{

    protected $store_id = 0;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var WariantyCollectionFactory
     */
    protected $wariantyCollectionFactory;

    protected $query;

    /**
     * @param CollectionFactory $collectionFactory
     * @param WariantyCollectionFactory $wariantyCollectionFactory
     * @param Query $query
     */
    public function __construct(
        CollectionFactory         $collectionFactory,
        WariantyCollectionFactory $wariantyCollectionFactory,
        Query                     $query
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->wariantyCollectionFactory = $wariantyCollectionFactory;
        $this->query = $query;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        try {
            $productIds = $observer->getProductIds();

            $attributes = $observer->getAttributesData();
            $data = $observer->getData();

            file_put_contents("/home/skando/public_html/pub/_mass_attr.txt", "Atrybuty: " . print_r($attributes, true));

            if (array_key_exists('wariant', $attributes)) {


                $new_wariants = $attributes['wariant'];
                $new_wariants_id = null;

                if ($new_wariants == 1) {
                    $new_wariants_id = $this->createNewWariant();
                    // to nadpisywało nowym wariantem w produktach w multiselect
//                    $attributes['wariant'] = $new_wariants;
//                    $observer->setAttributesData($attributes);

                    // wywalamy z masowego dodawania w produktach atrybutu wariant bo nadpisuje poprzednie warianty
                    unset($attributes['wariant']);
                    $observer->setAttributesData($attributes);
                }


                if (is_array($new_wariants)) {
                    if (($key = array_search(1, $new_wariants)) !== false) {
                        unset($new_wariants[$key]);
                    }
                }

                $collection = $this->collectionFactory->create();
                $collection->addAttributeToFilter('entity_id', ['in' => $productIds]);
                $collection->addAttributeToSelect('*');

                foreach ($collection as $product) {


                    if ($new_wariants_id) {
                        if ($curent_wariant = $product->getWariant()) {
                            $_wariants_to_update = $curent_wariant . ',' . $new_wariants_id;
                        } else {
                            $_wariants_to_update = $new_wariants_id;
                        }


                        $this->query->updateAttrVarchar($product->getSku(), $_wariants_to_update, 'wariant');
                    }

                    file_put_contents("_mass_wariant.txt", "SKU " . print_r($product->getSku() . " : " . $_wariants_to_update, true), FILE_APPEND);


                    $old_wariants = explode(",",);

                    if ($attributes['wariant'] != "") {


                        if ($product->getWariant() != "") {
                            if (!is_array($old_wariants)) {
                                $old_wariants = [$old_wariants];
                            }
                            if (!is_array($new_wariants)) {
                                $new_wariants = [$new_wariants_id];
                            }
                            $add_wariants = array_diff($new_wariants, $old_wariants);
                            $del_wariants = array_diff($old_wariants, $new_wariants);
                            if (!is_array($add_wariants)) {
                                $add_wariants = [$add_wariants];
                            }
                        } else {
                            $add_wariants = $new_wariants;
                            $del_wariants = [];

                            if (!is_array($add_wariants)) {
                                $add_wariants = [$add_wariants];
                            }
                        }

//                        file_put_contents("_mass_wariant.txt", "\nNEW ".print_r($add_wariants,true),FILE_APPEND);

                        // ADD
                        $wariantyCollection = $this->wariantyCollectionFactory->create();
                        $wariantyCollection->addFieldToFilter('warianty_id', ['in' => $add_wariants]);
                        foreach ($wariantyCollection as $wariant) {
                            $skus = explode(PHP_EOL, $wariant->getSkus());
                            if (!in_array($product->getSku(), $skus)) {
                                $skus[] = $product->getSku();
                                $newSkus = implode(PHP_EOL, $skus);
                                $wariant->setSkus($newSkus);
                                $wariant->save();
                            }
//                            file_put_contents("_mass_wariant.txt", "\nSKUS ".print_r($skus,true),FILE_APPEND);
                        }


                        // DELETE
//                        $wariantyCollection = $this->wariantyCollectionFactory->create();
//                        $wariantyCollection->addFieldToFilter('warianty_id', ['in' => $del_wariants]);
//                        foreach ($wariantyCollection as $wariant) {
//                            $skus = explode(PHP_EOL, $wariant->getSkus());
//                            if (($key = array_search($product->getSku(), $skus)) !== false) {
//                                unset($skus[$key]);
//                                $newSkus = implode(PHP_EOL,$skus);
//                                $wariant->setSkus($newSkus);
//                                $wariant->save();
//                            }
//                        }
                    } else {

                        file_put_contents("_mass_wariant.txt", "\nREMOVE ALL " . print_r($old_wariants, true), FILE_APPEND);
                        // REMOVE ALL
                        $wariantyCollection = $this->wariantyCollectionFactory->create();
                        $wariantyCollection->addFieldToFilter('warianty_id', ['in' => $old_wariants]);
                        foreach ($wariantyCollection as $wariant) {
//                            file_put_contents("_mass_wariant.txt", "\nWARIANT ".print_r($wariant->getId(),true),FILE_APPEND);
                            $skus = explode(PHP_EOL, $wariant->getSkus());
                            if (($key = array_search($product->getSku(), $skus)) !== false) {
                                unset($skus[$key]);
                                $newSkus = implode(PHP_EOL, $skus);

                                $wariant->setSkus($newSkus);
                                $wariant->save();
                            }
                        }
                    }
                }
            }


        } catch (\Execption $e) {
            file_put_contents("/home/skando/public_html/pub/_err_mass_wariant.txt", $e->getMessage(), FILE_APPEND);
        }
    }

    private function createNewWariant()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create(\Kowal\Warianty\Model\Warianty::class)->load(null);
        $data = ['wariant_name' => date('Ymd-His') . ' wariant'];
        $model->setData($data);
        $model->save();
        return $model->getId();
    }

}
