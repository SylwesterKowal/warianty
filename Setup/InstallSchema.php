<?php
/**
 * Warianty produktów
 * Copyright (C) 2019  kowal sp. z o.o.
 *
 * This file included in Kowal/Warianty is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace Kowal\Warianty\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface   $setup,
        ModuleContextInterface $context
    )
    {
        $table_kowal_warianty_warianty = $setup->getConnection()->newTable($setup->getTable('kowal_warianty_warianty'));

        $table_kowal_warianty_warianty->addColumn(
            'warianty_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true,],
            'Entity ID'
        );

        $table_kowal_warianty_warianty->addColumn(
            'wariant_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'wariant_name'
        );

        $table_kowal_warianty_warianty->addColumn(
            'skus',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => False],
            'skus'
        );

        $table_kowal_warianty_warianty->addColumn(
            'grupa',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => True],
            'grupa'
        );

        $table_kowal_warianty_warianty->addColumn(
            'swatch',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => True],
            'Display only value'
        );


        $table_kowal_warianty_warianty->addColumn(
            'maingroup',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => True],
            'Display only value'
        );

        $setup->getConnection()->createTable($table_kowal_warianty_warianty);
    }
}
