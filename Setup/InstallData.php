<?php
/**
 * Warianty produktów
 * Copyright (C) 2019  kowal sp. z o.o.
 *
 * This file included in Kowal/Warianty is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace Kowal\Warianty\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface
{

    private $eavSetupFactory;
    private $storeManager;
    private $scopeConfig;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory                                    $eavSetupFactory,
        \Magento\Store\Model\StoreManagerInterface         $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface   $context
    )
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'wariant',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Warianty produktu',
                'input' => 'multiselect',
                'class' => '',
                'source' => \Kowal\Warianty\Model\Product\Attribute\Source\Wariant::class,
                'global' => 0,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'General',
                'option' => ''
            ]
        );

        $this->storeManager->reinitStores();

//        $connection = $setup->getConnection();
//        foreach ($this->storeManager->getStores() as $store) {
//            try {
//                $data['warianty_id'] = 1;
//                $data['wariant_name'] = '+ Stwóż nowy wariant';
//                $connection->insert($setup->getTable('kowal_warianty_warianty'), $data);
//            } catch (\Exception $e) {
//
//            }
//        }
    }
}
